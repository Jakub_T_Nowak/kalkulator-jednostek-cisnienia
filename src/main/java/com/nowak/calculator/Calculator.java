package com.nowak.calculator;

public class Calculator {
    private Double value;
    private String in;
    private String out;

    public Calculator(Double value, String in, String out) {

        setValue(value);

        this.in = in;
        this.out = out;
    }

    public void setValue(Double value) {

        if (value == null || (value < 0 || value > 10000000))
            this.value = 0.0;
        else
            this.value = value;
    }


    private double convert(Double value, String in, String out) {

        if (in.equals("Pa")) {
            return paTo(value, out);
        }
        if (in.equals("kPa")) {
            return paTo(value, out) * 1000;
        }
        if (in.equals("MPa")) {
            return paTo(value, out) * 1000 * 1000;
        }
        if (in.equals("mH2O")) {
            return mmh2oTo(value, out);
        }
        if (in.equals("cmH2O")) {
            return mmh2oTo(value, out) * 10;
        }
        if (in.equals("mmH2O")) {
            return mmh2oTo(value, out) * 100;
        }
        return 0.0;
    }

    private Double paTo(Double value, String out) {

        if (out.equals("Pa")) {
            return value;
        } else if (out.equals("kPa")) {
            return value * 0.001000;
        } else if (out.equals("MPa")) {
            return value * 0.000001;
        } else if (out.equals("mH2O")) {
            return value * 0.10197442889 * 0.001;
        } else if (out.equals("cmH2O")) {
            return value * 0.10197442889 * 0.1;
        } else if (out.equals("mmH2O")) {
            return value * 0.10197442889;
        }
        return 0.0;
    }

    private Double mmh2oTo(Double value, String out) {
        if (out.equals("Pa")) {
            return value * 9.806380;
        } else if (out.equals("kPa")) {
            return value * 9.806380 * 0.001;
        } else if (out.equals("MPa")) {
            return value * 9.806380 * 0.000001;
        } else if (out.equals("mH2O")) {
            return value * 100;
        } else if (out.equals("cmH2O")) {
            return value * 10;
        } else if (out.equals("mmH2O")) {
            return value;
        }
        return 0.0;
    }


    @Override
    public String toString() {

        if (value != 0)
            return value + " " + in + " to " + String.format ("%.6f", convert(value, in, out)) + " " + out;
        else
            return "Podaj właściwą wartość wejściową!";

    }
}